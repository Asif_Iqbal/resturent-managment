<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo">
    <a href="{{route('welcome')}}" class="simple-text logo-normal">
       Resturent Managment
      </a>
    </div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li class="nav-item ">
          <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="material-icons">dashboard</i>
            <p>Dashboard</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{ route('slider.index') }}">
            <i class="material-icons">person</i>
            <p>Slider</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{ route ('slider.create')}}">
            <i class="material-icons">content_paste</i>
            <p>Upload Slider</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{ route('category.index') }}">
            <i class="material-icons">library_books</i>
            <p>Category</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{ route('item.show') }}">
            <i class="material-icons">bubble_chart</i>
            <p>Item</p>
          </a>
        </li>
        <li class="nav-item ">
        <a class="nav-link" href="{{route('reservation.show')}}">
            <i class="material-icons">location_ons</i>
            <p>Reservation</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{ route('message') }}">
            <i class="material-icons">notifications</i>
            <p>User Message</p>
          </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{ route('food.category') }}">
              <i class="material-icons">notifications</i>
              <p>Food category</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="{{ route('item') }}">
              <i class="material-icons">notifications</i>
              <p>Food Items</p>
            </a>
          </li>

      </ul>
    </div>
  </div>

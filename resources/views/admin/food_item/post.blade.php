@extends('layouts.app')
@section('content')
<div class="content">
    <div class="container">
<center><h1> Add Item</h1></center>

<div class="container">

<form class="form-horizontal" action="{{ route('item.food')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
  <fieldset>

        <div class="control-group">
                <label>Select food category:</label>

                <select  class="control" name="food_category_id">
                        @foreach ($categorys_food as $category)
                  <option value="{{ $category->id }}">{{ $category->name }}</option>
                  @endforeach
                </select>

              </div>

              <div class="control-group">
                <label class="control-label">Food item name</label>
                <div class="controls">
                  <input type="text" name="name" placeholder="item_name" required="">
                </div>
            </div>
      <div class="control-group">
        <label class="control-label">Description</label>
        <div class="controls">
          <input type="text" name="body" placeholder="item_name" required="">
        </div>
    </div>

    <div class="control-group">
            <label class="control-label">price</label>
            <div class="controls">
              <input type="text" name="price" placeholder="price" required="">
            </div>
        </div>


    <div class="form-actions">
      <button type="submit" class="btn btn-primary">Add </button>
      <button type="reset" class="btn">Cancel</button>
    </div>

  </fieldset>
</form>
</div>
</div>


</div>

@endsection

@extends('layouts.app')
@section('content')
<div class="content">
 <a class="btn btn-info" href="{{ route('item.show') }}">Add food Item</a>
<center><h1>Food Category</h1></center>

<table class="table">
<tr>
    <th>Id</th>
    <th>Category</th>
    <th>Body</th>
    <th>Price</th>

</tr>
@foreach ($categorys_food as $key=>$category)
<tr>
    <td>{{ $key+1 }}</td>
    <td>{{ $category->food_category->name }}</td>
    <td>{{ $category->body }}</td>
    <td>{{ $category->price }}</td>
    <td><a href=""  class="btn btn-danger"> Delete</a>
       </td>

</tr>
@endforeach

</table>
</div>

@endsection

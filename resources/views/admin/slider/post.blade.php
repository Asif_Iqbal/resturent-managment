@extends('layouts.app')
@section('content')
<div class="content">
<center><h1>Slider Upload</h1></center>

<div class="container">

<form class="form-horizontal" action="{{ route('slider.store')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
  <fieldset>

      <div class="control-group">
        <label class="control-label">Title of Slider</label>
        <div class="controls">
          <input type="text" name="title" placeholder="slider title" required="">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label">Title of Slider</label>
        <div class="controls">
          <input type="text" name="sub_title" placeholder="slider sub_title" required="">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label">Slider Image</label>
        <div class="controls">
          <input type="file" name="image" required="">
        </div>
    </div>

    <div class="form-actions">
      <button type="submit" class="btn btn-primary">Add Slider</button>
      <button type="reset" class="btn">Cancel</button>
    </div>

  </fieldset>
</form>
</div>


</div>

@endsection

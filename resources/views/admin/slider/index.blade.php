@extends('layouts.app')
@section('content')
<div class="content">
<center><h1>Slider</h1></center>

<table class="table">
<tr>
    <th>Id</th>
    <th>Title</th>
    <th>Sub-title</th>
    <th>Image</th>
</tr>
@foreach ($sliders as $key=>$slider)
<tr>
    <td>{{ $key+1 }}</td>
    <td>{{ $slider->title }}</td>
    <td>{{ $slider->sub_title }}</td>
    <td><img src="{{ asset('images/'.$slider->image) }}" alt="image" height="80px"></td>
    <td><a href="{{ route('slider.delete',$slider->id) }}" class="btn btn-danger"> Delete</a></td>
</tr>
@endforeach

</table>
</div>

@endsection

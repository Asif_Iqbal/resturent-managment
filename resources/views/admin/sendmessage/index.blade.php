@extends('layouts.app')
@section('content')
<div class="content">
<center><h1>User Message</h1></center>

<table class="table">
<tr>
    <th>Id</th>
    <th>Name</th>
    <th>Email</th>
    <th>Subject</th>
    <th>Message</th>
</tr>
@foreach ($messages as $key=>$message)
<tr>
    <td>{{ $key+1 }}</td>
    <td>{{ $message->name }}</td>
    <td>{{ $message->email}}</td>
    <td>{{ $message->subject }}</td>
    <td>{{ $message->message }}</td>
</tr>
@endforeach

</table>
</div>

@stop

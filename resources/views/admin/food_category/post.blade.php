@extends('layouts.app')
@section('content')
<div class="content">
<center><h1>Add Food Category </h1></center>

<div class="container">

<form class="form-horizontal" action="{{ route('category.food')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
  <fieldset>

      <div class="control-group">
        <label class="control-label"> Food Category name</label>
        <div class="controls">
          <input type="text" name="name" placeholder="category" required="">
        </div>
    </div>

    <div class="form-actions">
      <button type="submit" class="btn btn-primary">Add food</button>
      <button type="reset" class="btn">Cancel</button>
    </div>

  </fieldset>
</form>
</div>


</div>

@endsection

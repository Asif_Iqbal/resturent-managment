@extends('layouts.app')
@section('content')
<div class="content">
   <a class="btn btn-info" href="{{ route('item.show1') }}">Add Item</a>   <center><h1>Category</h1></center>
<center><h1>Item</h1></center>

<table class="table">
<tr>
    <th>Id</th>
    <th>Category</th>
    <th>Name</th>
    <th>Body</th>
    <th>Price</th>
    <th>Image</th>
</tr>
@foreach ($items as $key=>$item)
<tr>
    <td>{{ $key+1 }}</td>
    <td>{{ $item->category->name }}</td>
    <td>{{ $item->name }}</td>
    <td>{{ $item->body }}</td>
    <td>{{ $item->price }}</td>
    <td><img src="{{ asset('images/'.$item->image) }}" alt="image" height="80px"></td>
    {{-- <td><a href="{{ route('slider.delete',$slider->id) }}" class="btn btn-danger"> Delete</a></td> --}}
</tr>
@endforeach

</table>
</div>

@endsection

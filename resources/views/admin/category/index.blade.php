@extends('layouts.app')
@section('content')
<div class="content">
 <a class="btn btn-info" href="{{ route('category.create') }}">Add Category</a>
<center><h1>Category</h1></center>

<table class="table">
<tr>
    <th>Id</th>
    <th>Category</th>
    <th>Slug</th>
</tr>
@foreach ($categorys as $key=>$category)
<tr>
    <td>{{ $key+1 }}</td>
    <td>{{ $category->name }}</td>
    <td>{{ $category->slug }}</td>
    <td><a href="{{ route('category.delete',$category->id) }}"  class="btn btn-danger"> Delete</a>
       </td>

</tr>
@endforeach

</table>
</div>

@endsection

@extends('layouts.app')
@section('content')
<div class="content">
   <center><h1>Reservation</h1></center>


<table class="table">
<tr>
    <th>Id</th>
    <th>Name</th>
    <th>Phone</th>
    <th>Email</th>
    <th>Time</th>
    <th>Message</th>
    <th>Status</th>
</tr>
@foreach ($reservations as $key=>$reservation)
<tr>
    <td>{{ $key+1 }}</td>
    <td>{{ $reservation->name }}</td>
    <td>{{ $reservation->phone }}</td>
    <td>{{ $reservation->email_address }}</td>
    <td>{{ $reservation->time }}</td>
    <td>{{ $reservation->message }}</td>
    <td>
        @if ($reservation->status==false)
        <form   action="{{ route('reservation.update',$reservation->id) }}"method="POST"  >
            @csrf
            {{ method_field('POST') }}
     <input type="hidden" name="status" value="1">
            <button class="btn btn-info" type="submit"><i class="material-icons">close</i></button>
    </form>
    @else
    <button class="btn btn-info" type="submit"> <i class="material-icons">done</i></button>
        {{-- @else
        <form  action="{{ route('reservation.update',$reservation->id) }}"method="POST">
            @csrf
            {{ method_field('POST') }}
            <input type="hidden" name="status" value="0">
                   <button class="btn btn-danger" type="submit">Not confirm</button>
           </form> --}}
        @endif
    </td>
</tr>
@endforeach

</table>
</div>

@endsection

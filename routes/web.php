<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/dashboard', 'AdminController@dashboard');


Auth::routes();

Route::get('/', 'HomeController@index')->name('welcome');
Route::post('/reservation', 'AdminController@reservation')->name('reservation');
Route::post('/send_message', 'AdminController@sendmessage')->name('send.message');



Route::group(['prefix' => 'admin', 'middleware' => 'auth',], function () {
    Route::get('/dashboard', 'AdminController@dashboard')->name('dashboard');
    Route::resource('/slider', 'SliderController');
    Route::get('/delete/{id}', 'AdminController@delete')->name('slider.delete');
    Route::get('/category_delete/{id}', 'AdminController@category_delete')->name('category.delete');
    Route::resource('/category', 'CategoryController');
    Route::get('/item', 'AdminController@item')->name('item.show');
    Route::get('/item_show', 'AdminController@item_show')->name('item.show1');
    Route::post('/add_item', 'AdminController@add_item')->name('item.store');
    Route::get('/reservation_show', 'AdminController@reservation_show')->name('reservation.show');
    Route::POST('/reservation_update/{id}', 'AdminController@updated')->name('reservation.update');
    Route::get('/message', 'AdminController@message')->name('message');
    Route::get('/food_category', 'AdminController@food_category')->name('food.category');
    Route::post('/food_category_add', 'AdminController@food_category_add')->name('category.food');
    Route::get('/food_category_show', 'AdminController@food_category_show')->name('category.show');
    Route::get('/food_item', 'AdminController@food_item')->name('food.item');
    Route::post('/food_item_add', 'AdminController@food_item_add')->name('item.food');
    Route::get('/food_item_show', 'AdminController@food_item_show')->name('item');
});

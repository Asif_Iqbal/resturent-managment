<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resarvation extends Model
{
    protected $fillable = ['name', 'message', 'email_address', 'status', 'phone',];
}

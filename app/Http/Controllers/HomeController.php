<?php

namespace App\Http\Controllers;

use App\Slider;
use App\Category;
use App\Item;
use App\Food_category;
use App\Food_item;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::all();
        $items = Item::all();
        $categorys = Category::all();
        $food_category = Food_category::all();
        $food_item = Food_item::all();
        return view(' welcome', compact('sliders', 'items', 'categorys', 'food_category', 'food_item'));
    }
}

<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;
use App\Category;
use App\Slider;
use App\Item;
use App\Resarvation;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ReservationConfirm;
use Brian2694\Toastr\Facades\Toastr;
use App\Sendmessage;
use App\Food_category;
use App\Food_item;

class AdminController extends Controller
{
    public function dashboard()
    {
        $categorys = Category::count();
        $sliders = Slider::count();
        $items = Item::count();
        $messages = Sendmessage::count();
        $reservations = Resarvation::count();
        return view('admin.dashboard', compact('categorys', 'sliders', 'items', 'messages', 'reservations'));
    }
    public function delete($id)
    {
        Slider::findOrFail($id)->delete();
        return redirect('admin/slider');
    }
    public function category_delete($id)
    {
        Category::findOrFail($id)->delete();
        return redirect()->back();
    }

    public function item()
    {
        $items = Item::all();
        return view('admin.item.index', compact('items'));
    }

    public function item_show()
    {
        $categorys = Category::all();
        return view('admin.item.post', compact('categorys'));
    }
    public function add_item(Request $request)
    {
        $items = new Item;
        [
            'file' => 'max:500000',
        ];
        $items->category_id = $request->category_id;
        $items->name = $request->name;
        $items->body = $request->body;
        $items->price = $request->price;


        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('/images');
            $image->move($location, $filename);
            $items->image = $filename;
        }

        $items->save();
        Toastr::success('Item', 'Item added', ["positionClass" => "toast-top-center"]);
        return Redirect('admin/item_show')->with('message', 'slider added Successfully');
    }

    public function reservation(Request $request)
    {
        $reservations = new Resarvation;
        $reservations->name = $request->name;
        $reservations->phone = $request->phone;
        $reservations->email_address = $request->email_address;
        $reservations->time = $request->time;
        $reservations->message = $request->message;
        $reservations->status = false;
        $reservations->save();
        Toastr::success('Reservation', 'pleash wait for confirmation ', ["positionClass" => "toast-top-center"]);
        return redirect()->back();
    }
    public function reservation_show()
    {
        $reservations = Resarvation::all();
        return view('admin.reservation.index', compact('reservations'));
    }

    public function updated(Request $request, $id)
    {
        $reservations = Resarvation::find($id);
        $reservations->status = true;
        $reservations->update();
        Notification::route('mail', $reservations->email_address)
            ->notify(new ReservationConfirm());
        Toastr::success('update successfully', 'change and send the mail', ["positionClass" => "toast-top-center"]);
        return redirect()->back();
    }

    public function sendmessage(Request $request)
    {
        $reservations = new Sendmessage;
        $reservations->name = $request->name;
        $reservations->email = $request->email;
        $reservations->subject = $request->subject;
        $reservations->message = $request->message;
        $reservations->save();
        Toastr::success('Message', 'send the message', ["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }
    public function message()
    {
        $messages = Sendmessage::all();
        return view('admin.sendmessage.index', compact('messages'));
    }

    public function food_category()
    {
        $categorys_food = Food_category::all();
        return view('admin.food_category.index', compact('categorys_food'));
    }
    public function food_category_add(Request $request)
    {
        $categorys_food = new Food_category;
        $categorys_food->name = $request->name;
        $categorys_food->slug = str_slug($request->name);
        $categorys_food->save();
        Toastr::success('Message', 'Add the food category', ["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }
    public function food_category_show()
    {
        return view('admin.food_category.post');
    }

    public function food_item()
    {
        $categorys_food = Food_item::all();
        return view('admin.food_item.index', compact('categorys_food'));
    }
    public function food_item_show()
    {
        $categorys_food = Food_category::all();
        return view('admin.food_item.post', compact('categorys_food'));
    }

    public function food_item_add(Request $request)
    {
        $categorys_food = new Food_item;
        $categorys_food->food_category_id = $request->food_category_id;
        $categorys_food->name = $request->name;
        $categorys_food->body = $request->body;
        $categorys_food->price = $request->price;
        $categorys_food->save();
        Toastr::success('Message', 'Add the food item', ["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }
}

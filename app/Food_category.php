<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food_category extends Model
{

    protected $fillable = ['name', 'slug',];


    public function food_items()
    {

        return $this->hasMany(Food_item::class);
    }
}

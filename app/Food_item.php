<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food_item extends Model
{

    protected $fillable = ['food_category_id', 'name', 'body', 'price',];

    public function food_category()
    {
        return $this->belongsTo(Food_category::class);
    }
}

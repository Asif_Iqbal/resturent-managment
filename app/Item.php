<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['category_id', 'body', 'name', 'body', 'price', 'image',];

    public function category()
    {

        return $this->belongsTo(Category::class);
    }
}
